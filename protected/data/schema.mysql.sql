-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: score
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'WEB'),(2,'NETWORKING'),(3,'CRYPTO');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `challenge`
--

DROP TABLE IF EXISTS `challenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `challenge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `desc` text NOT NULL,
  `flag` text NOT NULL,
  `score` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `challenge`
--

LOCK TABLES `challenge` WRITE;
/*!40000 ALTER TABLE `challenge` DISABLE KEYS */;
INSERT INTO `challenge` VALUES (1,3,'HaveYouSeenMyKey','<p>\r\n	<span style=\"font-size:22px;\"><span style=\"color:#ff0000;\"><strong>Well</strong></span>, here you have to decrypt a file, but wait!! You have weapons<br />\r\n	to help =D . So here is the challenge, find the file &quot;clear1&quot; !<br />\r\n	Hint: Study the code, bad bad key generation ;)<br />\r\n	<br />\r\n	link: <a href=\"/files/weapons.zip\">download </a></span></p>\r\n<p>\r\n	<span style=\"font-size:22px;\">this is what you need &nbsp;to do ;)&nbsp;</span></p>\r\n<p style=\"text-align: center;\">\r\n	<span style=\"color:#ffa500;\"><span style=\"font-size: 72px;\"><span style=\"text-align: center;\">&reg;</span></span></span></p>\r\n<p>\r\n	<span style=\"font-size:22px;\">see you next time little <span style=\"background-color:lime;\">hacker</span> ;)</span></p>\r\n<p>\r\n	<span style=\"font-size:22px;\">we will use this shit next time :D</span></p>\r\n','KC57KC57KC57KC57FLAG=DecryptTheFlagToGetTheKeyLolKC57KC57KC?KC57KC57KC57KC57KC57',1000),(2,3,'DigFurther','<p>\r\n	<span style=\"color:#ff0000;\"><span style=\"font-family: comic sans ms,cursive;\"><strong>No story now -_-</strong></span></span><br />\r\n	<br />\r\n	The cipher is {276;190;125;201;276;345;187;243 ;313 ; 252 ; 150 ;99 }<br />\r\n	HINT: The message has been encrypted with knapsack algorithm.An Asymetric algorithm.</p>\r\n<p>\r\n	Here is the Private key {2,3,6,13,27,52}<br />\r\n	KnapSack encrypts using the private key, and two numbers, &quot;n&quot; equals in this case to 31<br />\r\n	and &quot;m&quot; equals to 105</p>\r\n<p>\r\n	Decrypt the message, by finding the inverse process &quot;multiply each of the ciphertext values<br />\r\n	by n^-1 mod m, and then partition with the private knapsack to get the plaintext<br />\r\n	values. n^-1 such as the rest of n(n^-1 ) devided by 105 equals 1 (modulo :3 )<br />\r\n	convert the flag to ascii ;)</p>\r\n','hackitni!',300),(3,3,'SumOrGo','<br># Question - Can you calculate the sum ? \r\n<br># Start reading from right\r\n<br># if digit then add the digit\r\n<br># if \'A\' then remove it and go back 2 places\r\n<br># if \'B\' then remove and go front 2 places\r\n<br># Example : 65a43b21 Answer is 14\r\n<br>#\r\n<br># Solve: 9798781ba143567b89784351b36769\r\n<br>689a4949877543623467b8776a424b34a4556787\r\n<br>a6542a213457a8865432a5b46569342578ab235912a\r\n<br>3985674a2345321895ba01987654032165a43b21','574',200),(4,3,'Puzzle','<br>\r\n<br>The Puzzle Challenge\r\n\r\n<br>Answer All the 1000 equations to get the flag =D\r\n<br>192.168.0.100:8989','1AbZcXy32',200),(5,3,'SimpleOne','<br>So, here is a simple crypto cipher \r\n<a href=\"/files/crypto.bin\">Download</a>\r\n<br>No Hint here :p','NEVERUSEROTCIPHER',100),(6,1,'URL Checker','<br>you goal is to access to \r\n<br>http://192.168.0.100/web/0/secret.php\r\n<br>no hint :3 ','1edc4323Z',100),(7,1,'Me The ADMIN :\'(','<br >Find Try To see The ADmin Panel ! 3:)\r\n<br >http://192.168.0.100/web/1/','Mpols12qs9',50),(12,2,'Twitter Password ','Tom got in his neighbor\'s network and captured the following file, in aim to find his neighbor\'s twitter credentials . Jerry please HELP TOM! \r\n<br>\r\nThe flag must be on the following form (user@password)\r\n<br>\r\nlink: <a href=\"/files/twitter.pcap\">download </a><br>\r\n','usertest@password',50),(8,2,'VoIP espionage  ','The company you\'re working for gave you the following file that contains a Voip log file,\r\nthe manager want to retrieve all the possible information from that log file (source number, server Ip , dest Ip, + connection degeste)\r\nthe flag is the combination of the sourceNumber and the connection degest following this form (sourceNUM@connectiondegest)\r\n<br>HINT:RFC6873\r\n<br>\r\n<br>\r\nlink: <a href=\"/files/20524547495\">download </a><br>\r\n','13@6c13de87f9cde9c44e95edbb68cbdea9',100),(11,1,'SQLi Lover Level 1 ;)','you love sql injection ? <br />\r\n\r\ntry to hack me<br>\r\n\r\nhttp://192.168.0.100/web/2/list.php','youarethemasterofsql',1000);
/*!40000 ALTER TABLE `challenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score`
--

DROP TABLE IF EXISTS `score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `challnges` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score`
--

LOCK TABLES `score` WRITE;
/*!40000 ALTER TABLE `score` DISABLE KEYS */;
INSERT INTO `score` VALUES (1,6,5,'2013-03-23 09:11:47'),(2,4,7,'2013-03-23 09:37:43'),(3,6,7,'2013-03-23 10:28:43'),(4,12,2,'2013-03-23 11:25:16'),(5,5,3,'2013-03-23 11:27:56'),(6,6,12,'2013-03-23 12:06:05'),(7,11,7,'2013-03-23 12:07:32'),(8,9,12,'2013-03-23 12:13:09'),(9,4,12,'2013-03-23 12:22:40'),(10,11,12,'2013-03-23 12:47:48'),(11,7,12,'2013-03-23 13:08:20'),(12,9,6,'2013-03-23 13:13:30'),(13,6,4,'2013-03-23 13:17:01'),(14,11,4,'2013-03-23 13:41:08'),(15,8,3,'2013-03-23 14:10:04'),(16,12,3,'2013-03-23 14:13:20'),(17,9,3,'2013-03-23 14:17:37'),(18,11,3,'2013-03-23 14:44:47'),(19,1,1,'2013-03-24 21:40:21'),(20,1,2,'2013-03-24 21:41:04'),(21,1,3,'2013-03-24 21:41:21'),(22,1,4,'2013-03-24 21:41:34'),(23,1,5,'2013-03-24 21:41:46'),(24,1,6,'2013-03-24 21:42:06'),(25,1,7,'2013-03-24 21:42:18'),(26,1,12,'2013-03-24 21:42:57'),(27,1,11,'2013-03-24 21:43:37');
/*!40000 ALTER TABLE `score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `pass` varchar(32) CHARACTER SET utf8 NOT NULL,
  `score` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'','admin','piwpiw',3000),(4,'','STOPING DEVILS','b9562',100),(5,'','g1','8b83f',200),(6,'','0xFFFFFF','4e836',400),(7,'','icy','8ce17',50),(8,'','Mostwanted','1cdfe',200),(9,'','*SSITEAM*','e185e',350),(10,'','collab','be6f1',0),(11,'','rana mhakyin','9iw99',500),(12,'','piw','piw',0),(13,'','hamza','hamza',0),(14,'','hamza','hamza',0),(15,'','hamza','hamza',0),(16,'','hamza','hamza',0),(17,'hamza@hamza.com','hamza2','hamza',0),(18,'net9et@gmail.com','<h1>H</h1>','hamza',0);
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-04-23 20:31:13
