<?php

class ChallengeController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('view','index'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);

        //challnges valid ?
        $score = Score::model()->count('tid=:tid AND challnges=:ch', array(':tid' => yii::app()->user->id, ':ch' => (int) $id));


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Challenge'])) {

            if ($model->flag == $_POST['Challenge']['flag'] AND !$score) {
                //save the score
                $score = new Score;
                
                $score->time = new CDbExpression('NOW()');
                $score->tid = yii::app()->user->id;
                $score->challnges = $id;
                $score->save();
                
                //add the point to the team 
                $team=Team::model()->findByPk(Yii::app()->user->id);
                $team->score=$team->score+$model->score;
                $team->save();
                
                Yii::app()->user->setFlash('flagok', 'Bravo Mr Geek +' . $model->score . ' points ');
            } else {
                Yii::app()->user->setFlash('flagno', 'Are you kidding Me ! ');
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('view', array(
            'model' => $model,
            'score' => $score,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Challenge');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Challenge the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Challenge::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}
