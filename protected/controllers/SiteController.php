<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'login', 'log', 'rank'),
                'users' => array('*'),
            ),
            /* array('deny',
              'actions'=>array('login'),
              'users'=>array('@'),
              ), */

            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'showlog', 'logout'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'cid=:cid';
        $criteria->params = array(':cid' => 1);
        $dataProvider1 = new CActiveDataProvider('Challenge');
        $dataProvider1->setCriteria($criteria);

        $criteria = new CDbCriteria;
        $criteria->condition = 'cid=:cid';

        $criteria->params = array(':cid' => 2);
        $dataProvider2 = new CActiveDataProvider('Challenge');
        $dataProvider2->setCriteria($criteria);

        $criteria = new CDbCriteria;
        $criteria->condition = 'cid=:cid';

        $criteria->params = array(':cid' => 3);
        $dataProvider3 = new CActiveDataProvider('Challenge');
        $dataProvider3->setCriteria($criteria);

        $this->render('index', array(
            'dataProvider1' => $dataProvider1,
            'dataProvider2' => $dataProvider2,
            'dataProvider3' => $dataProvider3,
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionlog() {

        $sql = 'select DATE_FORMAT(time,\'%d%H%i\') as date , s.tid ,t.name as team_name,c.score as data ,c.name as challenge_name from score s,challenge c,team t where s.tid=t.id and c.id=s.challnges order by s.id';

        $sql_data = Yii::app()->db->createCommand($sql)->queryAll();

        //order data by team :)
        $data = array();

        $date = array();

        foreach($sql_data as $item ){
    
            @$data[$item['team_name']][$item['date']] = end($data[$item['team_name']]) + intval($item['data']);
            
            @$date[$item['date']] = 0;
        }
        $date['999999']=0;

        //the structer of the chartjs
        foreach ($data as $key => $value) {
            $value = $value+$date;
            ksort($value);
            //full the gaps
            $last=0;
            foreach ($value as $date_key => $v) {
                if($v==0){
                    $value[$date_key]=$last;
                }else{
                    $last=$v;
                }
            }

            $chart[]=array('name'=>$key,'data'=>array_values($value));
        }
        
        $this->render('log', array(
            'data' => $chart,
        ));
    }

    public function actionRank() {

        $sql = ' SELECT `name`, `score` as `data` FROM `team` `t` LEFT JOIN `score` `s` ON s.tid=t.id GROUP BY `t`.`name` ORDER BY `t`.`score` DESC, `s`.`time` ASC ';

        $chart = Yii::app()->db->createCommand($sql)->queryAll();
        
        foreach ($chart as $key => $value) {
            $chart[$key]['data']=array(intval($value['data']));
        }
        
        $this->render('rank', array(
            'data' => $chart,
        ));
    }
}