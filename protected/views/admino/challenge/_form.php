<?php
/* @var $this ChallengeController */
/* @var $model Challenge */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'challenge-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cid'); ?>
		<?php echo $form->textField($model,'cid'); ?>
		<?php echo $form->error($model,'cid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'flag'); ?>
		<?php echo $form->textArea($model,'flag',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'score'); ?>
		<?php echo $form->textField($model,'score',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desc'); ?>
	<?php $this->widget('application.extensions.editMe.ExtEditMe', array(
      'model'=>$model,
	  'attribute'=>'desc',
	  "height"=>'500px',
	  "width"=>'700px',
     "toolbar"=>array(
		   array('Undo','Redo','-','Find','Replace','-','SelectAll'),
		   array('/'),
		   array('Color','Bold','Italic','Underline','Strike','-','Subscript','Superscript'),
		   array('NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'),
		   array('JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),
		   array('BidiLtr', 'BidiRtl' ),
                   array('TextColor', 'BGColor'), 		
		   array('Link','Unlink','Anchor'),
		   array('Table','HorizontalRule','SpecialChar','PageBreak'),
		   array('/'),
		   array('Styles','Format','Font','FontSize'),
		   array('/'),
		   array('Source'),
	   ),
   )); ?>
		<?php echo $form->error($model,'desc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
