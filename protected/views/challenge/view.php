<?php
/* @var $this ChallengeController */
/* @var $model Challenge */

$this->breadcrumbs = array(
    'Challenges' => array('/'),
    $model->name,
);
?>
<h1><?php echo $model->name; ?></h1>

<?php if (Yii::app()->user->hasFlash('flagok')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('flagok'); ?>
    </div>
    <?php
    return 1;
endif;
?>

<?php if (Yii::app()->user->hasFlash('flagno')): ?>

    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('flagno'); ?>
    </div>
    <?php
endif;
?>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'score',
        'desc:html',
    ),
));
?>
<?php if (!$score): ?>
    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'challenge-form',
            'enableAjaxValidation' => false,
                ));
        $model->flag = NULL;
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'flag'); ?>
            <?php echo $form->textField($model, 'flag', array('size' => 50)); ?>
            <?php echo $form->error($model, 'flag'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Send'); ?>
        </div>

        <?php $this->endWidget(); ?>

    <?php else: ?>

        Challenge Complited

    <?php endif; ?> 
</div><!-- form -->
