<?php
/* @var $this TeamController */
/* @var $model Team */

$this->breadcrumbs = array(
    'Score Board',
);
?>

<link rel="stylesheet" type="text/css" href="/assets/e9552bb5/gridview/styles.css" />

<h1>ScoreBoard</h1>

<div id="team-grid" class="grid-view">
    <table class="items">
        <thead>
            <tr><th>Rank</th><th>Team</th><th>Score</th></tr>
        </thead>
        <tbody>
            <?php
            foreach ($order as $item):
                $i++;
                $c = ($c == 'odd') ? 'even' : 'odd';
                ?>
                <tr class="<?php echo $c; ?>">
                    <td><?php echo $i; ?></td><td><?php echo $item['name'] ?></td><td><?php echo $item['score'] ?></td></tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
