<?php
/* @var $this SiteController */

$this->breadcrumbs = array(
    'Score Board',
);
?>
<h1>ScoreBoard</h1>

<?php

$this->Widget('ext.highcharts.HighchartsWidget', array(
   'options'=>array(
    'chart'=> array('type'=> 'column'),
      'title' => array('text' => 'Teams Rank'),
      'xAxis' => array('type' => 'category'),
      'yAxis' => array('title' => array('text' => 'score')),
      'legend' => array('enabled' => false),
      'tooltip'=>array(
                    'headerFormat'=> '<b>{series.name}</b><br>',
                    'pointFormat'=> 'score : <b>{point.y:.2f}</b><br/>'
                ),
      'series' => $data,
   )
));