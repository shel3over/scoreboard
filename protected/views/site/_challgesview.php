<?php
/* @var $this ChallengeController */
/* @var $data Challenge */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->name), array('/challenge/' . $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('score')); ?>:</b>
    <?php echo CHtml::encode($data->score); ?>
    <br />
</div>
