<?php
/* @var $this TeamController */
/* @var $data Team */
?>

<div class="view">

	<b><?php echo CHtml::encode($data['score']); ?>:</b>
	<br />

	<b>:</b>
	<?php echo CHtml::encode($data['team_name']); ?>
	<br />
</div>
