<?php
/* @var $this TeamController */
/* @var $model Team */

$this->breadcrumbs = array(
    'Score Board',
);
?>
<h1>Score Logs</h1>

<?php
$this->Widget('ext.highcharts.HighchartsWidget', array(
   'options'=>array(
      'title' => array('text' => 'Score Logs'),
      'yAxis' => array(
         'title' => array('text' => 'Score')
      ),
      'series' => $data,
   )
));
?>


